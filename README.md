# laravel-appconfig

#### 介绍
自定义一个配置服务[缓存-数据库-默认配置]
数据库层可以没有。
但是如果不定义模型 ，
批量获取可能达不到你想要的效果

### 安装教程

```shell

# 安装

$ composer require stevema/laravel-appconfig

```


### 使用说明

```php
# 1、生成配置文件
$ php artisan vendor:publish --tag="appconfig"

# 2、执行数据迁移 - 生成 app_configs 表
$ php artisan migrate

# 3、修改配置文件 /config/appconfig.php 或在 /.env 文件中添加配置
APPCONFIG_MODEL=  # APPCONFIG用到的数据模型
```

### 目录说明

```
├─ src
│   ├─ Config               # 配置文件目录
│   │   └─ config.php       # 配置文件
│   ├─ Facades              # 门面文件目录
│   │   └─ Appconfig.php    # 门面
│   ├─ Migrations           # 数据库迁移文件
│   │   └─ create_app_configs_table.php     # 迁移文件
│   └─ AppConfigException.php   # 异常文件
│   └─ AppConfigManage.php      # 项目主文件
│   └─ AppConfigModel.php       # 模型文件
│   └─ AppConfigProvider.php    # 服务者
│   └─ helpers.php          # 帮助文件
└─ composer.json            # 配置

```

### 使用方式

```php
# 引用门面
use Stevema\Facades\AppConfig;

# 随便加一个路由
Route::get('/t/config', function(){
    # 定义 一些变量
    $type = 'type';
    $values = [
        'name3' => 'value3',
        'name4' => 'value4',
        'name5' => 'value5',
    ];
    $name = 'name6';
    $value = 'value6';
    # 先设置一些
//    $config = app('appconfig');
    $config = appconfig();

    $config->setValues($type, $values);
//    appconfig_set_values($type, $values);
//    AppConfig::setValues($type, $values);

    # 设置一个
    $config->set($type, $name, $value);
//    AppConfig::set($type, $name, $value);
//    appconfig_set($type, $name, $value);

    # 获取一个
    $value = $config->get($type, $name, 'default_value');
//    $value = AppConfig::get($type, $name, 'default_value');
//    $value = appconfig($type, $name, 'default_value');
    dump($value);

    # 批量获取 如果没定义模型 批量获取可能达不到你想要的效果
    $values = $config->get($type);
//    $values = AppConfig::get($type);
//    $values = appconfig($type);
    dd($values);
});

```


### 备注

还是有优化的地方-先这样吧

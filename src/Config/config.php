<?php
use Stevema\AppConfig\AppConfigModel;
return [
    /*
    |--------------------------------------------------------------------------
    | AppConfig 配置
    |--------------------------------------------------------------------------
    |
    | 先走缓存,再走Model,最后找配置
    |
    */

    // 对应的模型  id、source、type、name、value
    'model' => env('APPCONFIG_MODEL', AppConfigModel::class),
    // 默认配置
    'default' => [
        'type' => [
            'name1' => 'value1',
            'name2' => 'value2',
        ],
    ]
];

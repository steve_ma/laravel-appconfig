<?php

namespace Stevema\AppConfig;

use Illuminate\Database\Eloquent\Model;

class AppConfigModel extends Model
{
    protected $table="app_configs";

    /**
     * 批量赋值的字段
     * @var string[]
     */
    protected $fillable = ['source','type','name','value'];
}

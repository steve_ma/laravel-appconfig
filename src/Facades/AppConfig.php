<?php
namespace Stevema\Facades;

use Illuminate\Support\Facades\Facade;
class AppConfig extends Facade
{
    /**
     * Get the name of the class registered in the Application container.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'appconfig';
    }
}

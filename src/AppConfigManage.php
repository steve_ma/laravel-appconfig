<?php
namespace Stevema\AppConfig;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
class AppConfigManage{
    public function getModel(){
        $modelName = Config::get('appconfig.model');
        if(empty($modelName)) return null;
        return $modelName;
    }

    /**
     * @param string $type
     * @param string|null $name
     * @param mixed $value
     * @param mixed $source
     * @return mixed
     */
    public function set(string $type, string $name = null, mixed $value = '', mixed $source = 'default'): mixed
    {
        $cacheKey = 'config' . ':' . $source. ':' . $type . '-' . $name;
        Cache::delete($cacheKey);
        $original = $value;
        $update_time = time();
        if (is_array($value)) {
            $value = json_encode($value, true);
        }
        $modelName = $this->getModel();
        if(! is_null($modelName)) {
            $data = $modelName::where(['type' => $type, 'name' => $name, 'source' => $source])->find();
            if (empty($data)) {
                $modelName::create([
                    'type' => $type,
                    'name' => $name,
                    'value' => $value,
                    'source' => $source
                ]);
            } else {
                $modelName::update([
                    'value' => $value,
                    'update_time' => $update_time
                ], ['type' => $type, 'name' => $name, 'source' => $source]);
            }
        }

        Cache::set($cacheKey, ['appconfig' => $value]);
        return $original;
    }

    /**
     * @param string $type
     * @param array $values
     * @param mixed $source
     * @return array
     */
    public function setValues(string $type, array $values, mixed $source='default'): array
    {
        if(!empty($values)){
            foreach($values as $name => $value){
                $this->set($type, $name, $value, $source);
            }
        }
        return $values;
    }

    /**
     * @param string $type
     * @param string|null $name
     * @param $defaultValue
     * @param mixed $source
     * @return mixed
     */
    public function get(string $type, string $name = null, $defaultValue = null, mixed $source = 'default'): mixed
    {
        //有缓存取缓存
        $cacheKey = 'config' . ':' . $source. ':' . $type . '-' . $name;
        $result = Cache::get($cacheKey);
        $value = $result['appconfig'] ?? null;
        if ($value !== null) {
            return $value;
        }

        $modelName = $this->getModel();

        //单项配置
        if ($name) {
            if(!is_null($modelName)){
                $result = $modelName::where([
                    'type' => $type,
                    'name' => $name,
                    'source' => $source
                ])->value('value');

                //数组配置需要自动转换
                $json = json_decode($result, true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    $result = $json;
                }
            }
            //获取调用默认配置
            if ($result === NULL) {
                $result = $defaultValue;
            }
            //获取系统配置文件的配置
            if ($result === NULL) {
                $result = Config::get('appconfig.default.' . $type . '.' . $name);
            }
            Cache::set($cacheKey, ['appconfig' => $result]);
            return $result;
        }

        //多项配置
        $data = [];
        if(!is_null($modelName)) {
            $data = $modelName::where([
                'type' => $type,
                'source' => $source
            ])->column('value', 'name');
            if (is_array($data)) {
                foreach ($data as $k => $v) {
                    $json = json_decode($v, true);
                    if (json_last_error() === JSON_ERROR_NONE) {
                        $data[$k] = $json;
                    }
                }
            }
        }
        if ($data === []) {
            $data = $defaultValue;
        }
        if ($data === NULL) {
            $data = Config::get('appconfig.default.' . $type . '.' . $name);
        }

        Cache::set($cacheKey, ['appconfig' => $data]);
        return $data;

    }
}

<?php

if (!function_exists('appconfig')) {
    function appconfig($type=null, $name = null, $defaultValue = null, $source = 'default')
    {
        $appconfig = app('appconfig');
        if(is_null($type)) {
            return $appconfig;
        }
        return $appconfig->get($type,$name,$defaultValue,$source);
    };
}
if (!function_exists('appconfig_set')) {
    function appconfig_set(string $type, string $name = null, mixed $value = '', mixed $source = 'default'){
        return appconfig()->set($type, $name, $value, $source);
    };
}
if (!function_exists('appconfig_set_values')) {
    function appconfig_set_values(string $type, array $values=[], mixed $source = 'default'){
        return appconfig()->setValues($type, $values, $source);
    };
}

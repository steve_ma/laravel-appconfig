<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('app_configs', function (Blueprint $table) {
            $table->comment("App配置表");
            $table->id();
            $table->string("source", 100)->default("")->comment("源");
            $table->string("type", 50)->default("")->comment("类型");
            $table->string("name", 50)->default("")->comment("键");
            $table->text("value")->nullable()->comment("值");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('app_configs');
    }
};
